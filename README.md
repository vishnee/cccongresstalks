Analysis of Chaos Communication Congress talks.

# Usage

If you want to use the results of the script for research, the only thing you probably need is the end result:

`cccongresstalks.csv`

If you want to reproduce the results, you can delete the CSV (except 1984.csv
because it was written manually), ICS and TXT files and re-run the whole
operation by executing the script:

`./cccongresstalks.py`

I experienced some throttling/blocking from the CCC servers, so it is better to run the script through the Tor Network. All in all this is what I do on a regular test run:

`mv 1984.csv 1984; rm *day*; rm *csv; mv 1984 1984.csv ; rm *ics`

`time torify ./cccongresstalks.py`

On my system (ThinkPad X200s) the whole run takes about 20 minutes.

If you run into missing dependencies, refer to the Requirements section below.

# Explanation

The script tries to make a CSV file for each year *if it is not already in
the directory*, and in the end merges all years ordered by year to
cccongresstalks.csv. For the first years you have to manually put the
compiled CSV files in the directory, because I don’t know about good sources
to make those programmatically. For roughly the 1990s we parse HTML or plain
text schedules that were published on the CCC website. For roughly the 2000s
there are iCal and XML files that we download and parse out the interesting
bits to CSV.

# Requirements

* Beautiful Soup 4 library
* Requests library
* vobject library

## Debian

`apt install python3 python3 python-beautifulsoup python3-vobject python3-requests`
