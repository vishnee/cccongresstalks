% Der 33 Jahrerückblick
% Technology and Politics in Congress Talks, from 1984 to now
% Mel & Maxigas @ 33C3

# 

## 

![](images/talks-history.png)

## 

2C3 (1985) video:

ARD Tagesschau

## 

![](images/word-cloud.png)

## 

![](images/NSA.png)

# Data

## 

 * **Before 1992:**
      + *Congress archive* <small>`https://events.ccc.de/congress/`</small>
           - 403 Forbidden → web.archive.org
      + *Datenschleuder archive* <small>`https://ds.ccc.de/`, `http://offiziere.ch/trust-us/ds/`</small>
           - scanned PDFs without OCR, reviews, …

## 

`Datenschleuder #7 (1984)`

## 

`Datenschleuder #37 (1991)`

## 

Congress 1990 review.

##

<!-- a talk that sounds very technical but in the abstract more politics comes up -->

## 

 * **After 1992:**
 
      + *Human readable* HTML schedules: scraping <small>`“no regex harmed”`</small>

## 

![](images/1996-fahrplan-screenshot.png)

## 

 * **After 2002:**

      + *Machine readable* dumps: iCal, XML

##

![](images/2008-xml-screenshot.png)

## 

**→ repository**: <small>`https://gitlab.com/maxigas/cccongresstalks`</small>

# Methods

## Categories

 * For Technology (e.g. technique only): t
 * For Politics (e.g. societal implications too): p
 * For Others: o
 * For Nontalks: n
 * For Unknown: u

## Categorisation

 * 2307 talks in CSV
 * 3 humans categorise
 * ***Not*** according to conference tracks!
 * Categories try to reflect the **research question**…
 
##

![](images/categories.png)

## Tech vs. politics

 * Almost ***all*** congress talks include *technique*.
 * *Technique* can be (social) engineering, psychology, finance…
 * *Law* is the most problematic, but:
      + ***does the talk include how the law shapes society?***
 * *Political* talks refer to social groups and/or society as a whole.

<!-- <small> -->
<!-- Since most CCCongress talks has to do with technology, we should interpet this narrowly as “only technology”.  However, technology in the broad sense including everything concerned with technique: for instance, social engineering, psychology, design and ergonomics and not necessary socio-political.  The most problematic discipline for this consideration is law, but as long as a talk is not focused on the role of a policy in shaping society as a whole, we consider it technical. -->
<!-- </small> -->

<!-- <small> -->
<!-- Since most CCCongress talks has to do with technology, we should interpet this widely as “maybe technology but *also* societal aspects”.  However, explaining a vulnerability in a security system is not political in itself: it has to also include explicit reflection on how such insight (potentially) changes society.  This is shown by references to concepts – ideally in the title – that are recognisable from the social scientific literature, and conversely, out of scope of the O’Reilly books corpus.  Like before, talks on policy (data protection and systems exploitation) are the most difficult to categorise, and they are weighted according to their focus on lawyers’ terms vs. societal implications. -->
<!-- </small> -->

## Difficulties

 * ***Undefined*** talks:
      + 13C3 (1996): “THC++”, no abstract
 * ***Not talks***:
      + Hacker jeopardy, radio, theatre, DJ set…
 * Controvertial talks:
       + 25C3 (2008): “Objects as Software: \newline The Coming Revolution”

## 

> How physical compilers (CNC machines, laser cutters, 3D printers, etc) are changing the way we make things, how we think about the nature of objects. This talk will focus on the future of digital manufacturing, and how self-replicating machines will make this technology accessible to everyone: ushering in a new era of technological advance.

# Preliminary results

## Consistencies

***What is the CCCongress?***

→ More tech talks but almost as much as political.

## 

![](images/all-pie.png)

## Inconsistencies

 * *Numbers go down after 2007*: Why?
     + No correllation with the change of venue…

##

![](images/all_tags.png)

## Potential findings

 * *More participants does not mean more talks.*
 * *1989*: report in the Datenschleuder says that
 
> "there was only a small part of the congress that was dealing with hacker-specific topics."

##

![](images/t-or-p-mel.png)

<!-- ##  -->

<!-- ![](images/t-vs-p-fumiko.png) -->

# Future work

## Contribute

 1. Hack on the repo:
       + Improve the code,
       + categorise talks,
       + fork and repurpose…
 2. Point out more data sources:
       + Mentions of a printed *Congress Paper*?
       + Mentions (1989) of an *electronic newspaper*?
       + Mentions (1991 Datenschleuder #34) of a *Chaos Archiv*?
 3. ***Talk to us!***
       + Discussion: *Day 3 (29th) 16:00 @ Room A.2*

## Next steps

**Quantitative:**

 * Merge three categorised datasets (Fumiko, Mel, Maxigas).
 * Complete the dataset & clean up the code.
 * *Categorisation is only the first step.*
 
**Qualitative:**

 * *Qualitative (interview, focus group) data to complement quantitative results.*
 * Statistics as a consistent starting point for discussions.
 * More interesting questions: *relationship*, **co-articulation**, etc.

# 

## Thank you!

Questions, comments, etc.? Talk to us or…

 * `melanie.stilz@tu-berlin.de`
      + <small>5BE8 241D 888E 0C44 0EB2  2F3E 8DA7 0308 F934 654C</small>
 * `maxigas@anargeek.net`
      + <small>FA00 8129 13E9 2617 C614  0901 7879 63BC 287E D166</small>

> https://slides.metatron.ai/der-33-jahreruckblick

